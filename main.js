
const btnIP = document.querySelector(".btn-ip");
const textShowIP = document.querySelector(".ip");
const textShowLocation = document.querySelector(".location");

const ipify = "https://api.ipify.org/?format=json";
const ipApi = "http://ip-api.com/json/";
const neededFields = ["continent", "country", "regionName", "city", "district", "lat", "lon"];
const generateUrl = (ip) => `${ipApi}${ip}?fields=${neededFields.join(",")}&lang=en`;

btnIP.addEventListener("click", LocationDefine);

async function LocationDefine() {
    try {
        btnIP.disabled = true;
        btnIP.style.backgroundColor = "grey";
        render(textShowIP, "please wait");
        render(textShowLocation, "please wait");

        const ip = await ipDefine();
        render(textShowIP, ip);
        const location = await getResponse(generateUrl(ip));
        render(textShowLocation, location);

        btnIP.style.backgroundColor = "#4f5af4";
    } catch (error) {
        render(textShowLocation, "Error: " + error.message);
    } finally {
        btnIP.disabled = false;
    }
}

const getResponse = async (url) => {
    const response = await fetch(url);
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    }
    return response.json();
};
const ipDefine = async () => {
    const response = await getResponse(ipify);
    return response.ip;
};

function render(domElement, value) {
    if (typeof value === "string") {
        return (domElement.textContent = value);
    }
    if (typeof value === "object") {
        const ul = document.createElement("ul");
        const listContent = Object.entries(value);
        for (let [key, value] of listContent) {
            const li = document.createElement("li");
            !value ? (value = "no data") : value;
            li.textContent = key + ": " + value;
            ul.append(li);
        }
        domElement.textContent = "data loaded:";
        domElement.append(ul);
        return ul;
    }
}
